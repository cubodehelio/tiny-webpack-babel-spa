# Patagonian-webpack-babel-2015

Entorno de desarrollo basado en webpack y babel (**en progreso**) para aplicaciones ES2015.

## Instalar dependencias

```bash
$ npm install
```

## Buildear

```bash
$ webpack
```
Se puede ver el resultado en el directorio `./dist`. Dejar que se enfríe, incluir el `bundle.js` en un `index.html` y servir :P

```html
<html>
  <head>
    <meta charset="utf-8">
    <title>Patagonian-webpack-babel-2015</title>
  </head>
  <body>
    <script src="bundle.js"></script>
  </body>
</html>
```


## Configuración

* `config.js`: configuración de usuario general para el entorno.
* `webpack.config.js` configuración para el [CLI de webpack](http://webpack.github.io/docs/cli.html). Contiene todo lo relativo a la compilación/transpilación de la aplicación como archivos fuente y destino, configuración para los [loaders](http://webpack.github.io/docs/using-loaders.html) etc..

## Otras configuraciones

* `.babelrc`: archivo de configuración para el [CLI de babel](https://babeljs.io/docs/usage/cli/). Está **solo por conveniencia** por si se quiere utilizar el CLI de babel directamente sin pasar por webpack.
* `.eslintrc.json`: configuración para [eslint](http://eslint.org/) que contempla el uso de ES2015 con módulos CommonJS. La aplicación no está siendo linteada al compilar, sin envargo muchos IDEs lo soportan por default.
* `bs-config.js` configuración para [lite-server]( https://github.com/johnpapa/lite-server). Ver archivo para más info.
