const config = require('./config');

module.exports = {
  entry: `./${config.app.webpackEntryPoint}`,
  output: {
    filename: config.bundleName
  },

  // activar source-maps para debuguear el bundle final en el browser.
  devtool: 'source-map',

  // configuración que permite que en podamos importar módulos sin tener que
  // especificar su extensión por ejemplo: `import message from ./lib/message`
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.js', 'es6.js']
  },

  module: {

    // Array de loaders que se aplican automáticamente a los módulos (según
    // su extensión), antes de ser incluidos en el bundle final.
    loaders: [

      /**
       * Babel Loader (https://github.com/babel/babel-loader).
       *
       * Todos los archivos terminados en `.js` pasarán antes por
       * `babel-loader`. En el caso de querer excluir ciertos módulos para que
       * no pasen por este loader, podríamos especificar otra extensión de
       * archivo como por ejemplo `/\.es6.js?$/` o `/\.babel.js?$/`.
       */
      {
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: [/node_modules/],
        cacheDirectory: './wpcache',

        // Opciones específicas para babel: Ver:
        // https://babeljs.io/docs/usage/options
        query: {
          presets: ['es2015']
        }
      },
    ]
  }
};
