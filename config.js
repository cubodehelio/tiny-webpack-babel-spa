const src = 'src';
const dist = 'dist';

module.exports = {
  src,
  dist,
  app: {

    /**
     * The entry point for the webpack bundler.
     */
    'webpackEntryPoint': `${src}/index.js`
  },

  /**
   * Where the webpack bundle file will be saved.
   */
  bundleName: `${dist}/bundle.js`
};
