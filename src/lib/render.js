const render = (message) => {
  document.write(`<h1>${message}!</h1>`);
};

/**
 * export usando `default` permite no usar llaves a la hora de importar:
 * `import render from 'render'` VS `import {render} from 'render'`
 */
export default render;
