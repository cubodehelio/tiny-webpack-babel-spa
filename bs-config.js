const config = require('./config');

/**
 * [lite-server]( https://github.com/johnpapa/lite-server) es un servidor muy
 * simple pero muy útil basado en nodejs. Este archivo de configuración no es
 * estrictamente necesario porque solo basta con disparar `lite-server` en el
 * directorio que se quiere servir. Sin embargo si encuentra un archivo
 * bs-config.js toma esa configuración.
 *
 * $ npm install lite-server -g
 * $ lite-server
 */

module.exports = {
  port: 3000,
  files: `./${config.dist}/**/*.*`,
  server: { baseDir: `./${config.dist}` }
};
